import fetch from 'node-fetch'
import {existsSync, readFileSync, writeFileSync} from 'fs'
import pdfParse from 'pdf-parse'

async function downloadFile(url: string, file: string) {
    let res = await fetch(url)
    let buffer = await res.buffer()
    writeFileSync(file, buffer)
}

type Sheet = {
    code: string // e.g. MA 0321
    date: string // ISOString
}
type MatchDay = {
    month: number // 1..12
    day: number // 1..31
    matches: Match[]
}
type Match = {
    hour: number // 00..23
    minute: number // 00..59
    group: string // e.g. 男甲一
    round: number
    host: string
    guest: string
    venue: string
    host_score: number
    guest_score: number
}

async function readPDF(file: string) {
    let buffer = readFileSync(file)
    let data = await pdfParse(buffer)
    let lines = data.text.split('\n')
        .map(line => line.trim())

    let i = 0

    function take() {
        let line = lines[i]
        console.log(`[${i}]:`, line)
        i++
        return line
    }

    function peek() {
        return lines[i]
    }

    take() // skip empty line
    take() // skip empty line
    let code = take().replace('編號 :', '').trim()
    take() // skip header line
    take() // skip header line

    function toInt(label: string, string: string): number {
        let number = parseInt(string)
        if (Number.isNaN(number)) {
            console.error(`expected an integer for "${label}", got: ` + JSON.stringify(string))
            throw new Error('unexpected format')
        }
        return number
    }

    function takeScore() {
        let host_score: string
        let guest_score: string
        if (peek().includes(':')) {
            [host_score, guest_score] = take().split(':')
        } else {
            host_score = take()
            take() // skip ':'
            guest_score = take()
        }
        return {
            host_score: toInt('Match:host_score', host_score),
            guest_score: toInt('Match:guest_score', guest_score),
        }
    }

    function takeMatchAfterDate(options: { skip_venue: boolean }): Match {
        let line = take()
        console.log('[takeMatchAfterDate] first line:', line)
        let hour = line.substr(0, 2)
        let minute = line.substr(2, 4)
        let group = take()
        let round = take()
        line = take()
        let host = line
        let guest = line
        let venue = ''
        if (!options.skip_venue) {
            venue = take()
        }
        let {host_score, guest_score} = takeScore()
        return {
            hour: toInt('Match:hour', hour),
            minute: toInt('Match:minute', minute),
            group,
            round: toInt('Match:round', round),
            host,
            guest,
            venue,
            host_score,
            guest_score,
        }
    }

    console.log(lines)

    function takeMatchDay(): MatchDay {
        let date = take()
        let parts = date.split('月')
        let month = parts[0]
        let day = parts[1].split('日')[0]

        let matches: Match[] = []

        let match1 = takeMatchAfterDate({skip_venue: false})
        console.dir({match1}, {depth: 20})
        take() // skip weekday
        let match2 = takeMatchAfterDate({skip_venue: true})
        match2.venue = match1.venue

        matches = [match1, match2]

        return {
            month: toInt('Day:month', month),
            day: toInt('Day:day', day),
            matches,
        }
    }

    let matchDays: MatchDay[] = []
    for (; ;) {
        if (peek().includes('康樂及文化事務署 資助')) {
            // take() // skip page title on new page
            // take() // skip page title on new page
            // take() // skip row headers on new page
            // take() // skip update time
            while (!(peek().includes('月') && peek().includes('日'))) {
                take()
            }
        }
        let matchDay = takeMatchDay()
        console.dir({matchDay}, {depth: 20})
        matchDays.push(matchDay)
    }

    console.log({
        code,
        matchDays,
        next_1: take(),
        next_2: take(),
        next_3: take(),
        next_4: take(),
        next_5: take(),
        next_6: take(),
        next_7: take(),
        next_8: take(),
        next_9: take(),
    })
}

async function main() {
    let url = 'http://www.basketball.org.hk/images/League/2019/2019%20League%20-%20Master%20Schedule_MA.pdf'
    let file = '2019.pdf'
    if (!existsSync(file)) {
        await downloadFile(url, file)
    }
    let res = await readPDF(file)
    console.log(res)
}

main()
